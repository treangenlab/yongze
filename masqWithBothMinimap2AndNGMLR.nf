params.shortForwardReadPath=""
params.shortReverseReadPath=""
params.nanoporeReadPath=""
params.assemblyPath=""
params.outputDir=""
 
process nanoQC{
    """
    nanoQC --outdir $params.outputDir/nanoQCResult/ $params.nanoporeReadPath
    """
}

process vigi{
    output:
     stdout vigi
     
    """
    mkdir $params.outputDir/vigi
    python hybrid_pipeline_run.py $params.assemblyPath $params.outputDir/vigi $params.nanoporeReadPath.fa
    """
}

process sortSamToBamLong{
    input:
     val x from vigi
    
    output:
     stdout samtoolLong
    
    """
    cat $params.outputDir/vigi/*.bam > $params.outputDir/vigi/combined.bam
    samtools calmd $params.outputDir/vigi/combined.bam $params.assemblyPath
    samtools sort $params.outputDir/vigi/combined.bam -o $params.outputDir/vigi/sortedAlignment.bam
    """
}

process sniffles{
    input:
    val x from samtoolLong
    
    output:
    stdout snf
    
    """
    mkdir $params.outputDir/snifflesResult
    sniffles -m $params.outputDir/vigi/sortedAlignment.bam -v $params.outputDir/snifflesResult/sniffles.vcf -t 40 --tmp_file $params.outputDir/snifflesResult
    """
}

process fastqc{
    """
    mkdir $params.outputDir/fastQCResult
    fastqc $params.shortForwardReadPath $params.shortReverseReadPath -t 20 -o $params.outputDir/fastQCResult
    """
}

process bwa{
    
    output:
     stdout bwa
    

    """
    mkdir $params.outputDir/bwaResult
    bwa index $params.assemblyPath
    bwa mem $params.assemblyPath $params.shortForwardReadPath $params.shortReverseReadPath > $params.outputDir/bwaResult/bwaShort.sam
    """
}


process sortSamToBamShort{
    input:
     val x from bwa
    
    output:
     stdout samtoolShort
    
    """
    samtools sort -O bam -T alignment.sort -o $params.outputDir/bwaResult/sortedAlignment.bam $params.outputDir/bwaResult/bwaShort.sam
    """
}


process manta{
    input:
     val x from samtoolShort
     
    output:
     stdout manta
     
    """
    mkdir $params.outputDir/mantaResult
    samtools index  $params.outputDir/bwaResult/sortedAlignment.bam
    samtools faidx $params.assemblyPath
    configManta.py --bam $params.outputDir/bwaResult/sortedAlignment.bam --referenceFasta $params.assemblyPath --runDir $params.outputDir/mantaResult
    python2 $params.outputDir/mantaResult/runWorkflow.py
    """
}

process Survivor{
    input:
     val x from manta
     val y from snf
     
    output:
     stdout survivor
    
    """
    mkdir $params.outputDir/survivorResult
    gunzip -k $params.outputDir/mantaResult/results/variants/diploidSV.vcf.gz
    cp $params.outputDir/mantaResult/results/variants/diploidSV.vcf $params.outputDir/survivorResult
    cp $params.outputDir/snifflesResult/sniffles.vcf $params.outputDir/survivorResult
    ls $params.outputDir/survivorResult/*vcf > $params.outputDir/survivorResult/vcfCombined
    SURVIVOR merge $params.outputDir/survivorResult/vcfCombined 1000 2 1 1 0 15 $params.outputDir/survivorResult/survivorCombined.vcf
    """
    
}


