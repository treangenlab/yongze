params.shortForwardReadPath=""
params.shortReverseReadPath=""
params.nanoporeReadPath=""
params.assemblyPath=""
params.outputDir=""


/*
 *process minimap{ 
 *
 *    input:
 *    file params.assemblyPath
 *    params.nanoporeReadPath
 *
 *    output:
 * 
 *    """
 *    mkdir $params.outputDir
 *    mkdir $params.outputDir/minimap2Result
 *    minimap2 -a $params.assemblyPath $params.nanoporeReadPath > $params.outputDir/minimap2Result/alignment.sam -t 80  
 *    """
 *}
 */
 
process nanoQC{
    """
    nanoQC --outdir $params.outputDir/nanoQCResult/ $params.nanoporeReadPath
    """
}

process ngmlr{
    output:
     stdout ngmlr
     
    """
    mkdir $params.outputDir/ngmlrResult
    ngmlr -r $params.assemblyPath -q $params.nanoporeReadPath -o $params.outputDir/ngmlrResult/ngmlrAlignment.sam -t 40  
    """
}

process sortSamToBamLong{
    input:
     val x from ngmlr
    
    output:
     stdout samtoolLong
    
    """
    samtools sort -O bam -T alignment.sort -o $params.outputDir/ngmlrResult/sortedAlignment.bam $params.outputDir/ngmlrResult/ngmlrAlignment.sam
    """
}


process sniffles{
    input:
    val x from samtoolLong
    
    output:
    stdout snf
    
    """
    mkdir $params.outputDir/snifflesResult
    sniffles -m $params.outputDir/ngmlrResult/sortedAlignment.bam -v $params.outputDir/snifflesResult/sniffles.vcf -t 40 --tmp_file $params.outputDir/snifflesResult
    """
}

process fastqc{
    """
    mkdir $params.outputDir/fastQCResult
    fastqc $params.shortForwardReadPath $params.shortReverseReadPath -t 20 -o $params.outputDir/fastQCResult
    """
}

process bwa{
    
    output:
     stdout bwa
    

    """
    mkdir $params.outputDir/bwaResult
    bwa index $params.assemblyPath
    bwa mem $params.assemblyPath $params.shortForwardReadPath $params.shortReverseReadPath > $params.outputDir/bwaResult/bwaShort.sam
    """
}


process sortSamToBamShort{
    input:
     val x from bwa
    
    output:
     stdout samtoolShort
    
    """
    samtools sort -O bam -T alignment.sort -o $params.outputDir/bwaResult/sortedAlignment.bam $params.outputDir/bwaResult/bwaShort.sam
    """
}


process manta{
    input:
     val x from samtoolShort
     
    output:
     stdout manta
     
    """
    mkdir $params.outputDir/mantaResult
    samtools index  $params.outputDir/bwaResult/sortedAlignment.bam
    samtools faidx $params.assemblyPath
    configManta.py --bam $params.outputDir/bwaResult/sortedAlignment.bam --referenceFasta $params.assemblyPath --runDir $params.outputDir/mantaResult
    python2 $params.outputDir/mantaResult/runWorkflow.py
    """
}

process Survivor{
    input:
     val x from manta
     val y from snf
     
    output:
     stdout survivor
    
    """
    mkdir $params.outputDir/survivorResult
    gunzip -k $params.outputDir/mantaResult/results/variants/diploidSV.vcf.gz
    cp $params.outputDir/mantaResult/results/variants/diploidSV.vcf $params.outputDir/survivorResult
    cp $params.outputDir/snifflesResult/sniffles.vcf $params.outputDir/survivorResult
    ls $params.outputDir/survivorResult/*vcf > $params.outputDir/survivorResult/vcfCombined
    SURVIVOR merge $params.outputDir/survivorResult/vcfCombined 1000 2 1 1 0 15 $params.outputDir/survivorResult/survivorCombined.vcf
    """
    
}





